package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.CoatDao;
import beans.CoatDate;

/**
 * Servlet implementation class CoatDetailServlet
 */
@WebServlet("/CoatDeleteServlet")
public class CoatDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CoatDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		CoatDao coatdao = new CoatDao();
		CoatDate coat = coatdao.findById(id);

		request.setAttribute("coat", coat);
		request.setAttribute("id", id);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coatdelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
        int i = Integer.parseInt(id);

        CoatDao coatdao = new CoatDao();
		CoatDate c = new CoatDate(i);

		coatdao.delete(c);

		response.sendRedirect("MasterHomeServlet");
	}

}
