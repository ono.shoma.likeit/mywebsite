package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.CoatDao;
import beans.CoatDate;

/**
 * Servlet implementation class CoatRegistServlet
 */
@WebServlet("/CoatRegistServlet")
public class CoatRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CoatRegistServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coatregist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String coatname = request.getParameter("coatname");
		String hourprice = request.getParameter("hourprice");
		String time1 = request.getParameter("time1");
		String time2 = request.getParameter("time2");
		String address = request.getParameter("address");
		String tel = request.getParameter("tel");
		String access = request.getParameter("access");
		String img1 = request.getParameter("img1");
		String img2 = request.getParameter("img2");
		String img3 = request.getParameter("img3");
		String detail = request.getParameter("detail");
		String sports = request.getParameter("sports");

		int price = Integer.parseInt(hourprice);
		int sportsId = Integer.parseInt(sports);

		CoatDao coatDao = new CoatDao();
		CoatDate coatInfo = new CoatDate(coatname,price,time1,time2,address,tel,
				access,img1,img2,img3,detail,sportsId);
		coatDao.coatinsert(coatInfo);

		response.sendRedirect("MasterHomeServlet");

	}

}
