package ec;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import DAO.UserDao;
import beans.UserDate;

/**
 * Servlet implementation class UserRegistconfilm
 */
@WebServlet("/UserRegistconfirm")
public class UserRegistconfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegistconfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("id");
		String password = request.getParameter("pass");
		String name = request.getParameter("name");
		String birth = request.getParameter("birth");

		java.sql.Date  birth_date = java.sql.Date.valueOf(birth);
		try {
	        //ハッシュを生成したい元の文字列
	        String source = password;
	        //ハッシュ生成前にバイト配列に置き換える際のCharset
	        Charset charset = StandardCharsets.UTF_8;
	        //ハッシュアルゴリズム
	        String algorithm = "MD5";

	        //ハッシュ生成処理
	        byte[] bytes;

			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

	        String result = DatatypeConverter.printHexBinary(bytes);

		UserDao userDao = new UserDao();
		UserDate c = new UserDate(loginId, result, name, birth_date);
		userDao.insert(c);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registresult.jsp");
		dispatcher.forward(request, response);
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
