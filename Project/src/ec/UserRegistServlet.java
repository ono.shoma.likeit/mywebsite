package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserDao;
import beans.UserDate;

/**
 * Servlet implementation class UserInsertServlet
 */
@WebServlet("/UserRegistServlet")
public class UserRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegistServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//新規登録からの情報取得
		String loginId = request.getParameter("id");
		String password = request.getParameter("pass");
		String password2 = request.getParameter("pass2");
		String name = request.getParameter("name");
		String birth = request.getParameter("birth_date");

		//登録失敗
        boolean error = false;

        UserDao dao = new UserDao();
        UserDate user = dao.findByLoginId(loginId);
	        if (user != null) {
	        	error = true;
	        }
			if(!password.equals(password2)) {
				error = true;
			}
			if(loginId.equals("") || password.equals("") || password2.equals("") || name.equals("") || birth.equals("")) {
				error = true;
			}
			if(error) {
				request.setAttribute("errMsg", "入力された内容は正しくありません。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
				dispatcher.forward(request, response);

				return;
			}

		//登録成功
			request.setAttribute("id",loginId);
			request.setAttribute("pass",password);
			request.setAttribute("name",name);
			request.setAttribute("birth",birth);

			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registconfirm.jsp");
			dispatcher.forward(request, response);
	}

}
