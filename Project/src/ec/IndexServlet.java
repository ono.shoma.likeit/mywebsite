package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.CoatDao;
import beans.CoatDate;
import beans.UserDate;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDate user =(UserDate) session.getAttribute("userInfo");
		if(user == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
			CoatDao CoatDao = new CoatDao();
			//コート情報を取得
			List<CoatDate>coatList = CoatDao.findAll();

			//リクエストスコープにセット
			request.setAttribute("coatList", coatList);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		// ユーザ一覧情報を取得
		CoatDao coatDao = new CoatDao();

		String sports = request.getParameter("sports");
		String address = request.getParameter("address");
		int sportsId = Integer.parseInt(sports);

		List<CoatDate> coatList = coatDao.Info(sportsId, address);
		request.setAttribute("coatList", coatList);


		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);

	}

}
