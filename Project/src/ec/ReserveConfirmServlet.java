package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.CoatDao;
import DAO.ReserveDao;
import beans.CoatDate;
import beans.ReserveDate;

/**
 * Servlet implementation class ReserveConfirmServlet
 */
@WebServlet("/ReserveConfirmServlet")
public class ReserveConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReserveConfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String date = request.getParameter("date");
		String startTime = request.getParameter("time1");
		String endTime = request.getParameter("time2");
		String coatId = request.getParameter("id");
		String name = request.getParameter("name");
		String hourPrice = request.getParameter("price");

		int startTimeChange = Integer.parseInt( startTime.substring(0, 2));
		int endTimeChange = Integer.parseInt(endTime.substring(0, 2));

		 ReserveDao reservedao = new ReserveDao();
		 List<ReserveDate> reserveList = reservedao.reserve(coatId,date);
		 CoatDao coatDao = new CoatDao();
		 CoatDate coatInfo = coatDao.findById(coatId);

		 for(ReserveDate reserve : reserveList) {
		 int rotime = Integer.parseInt(reserve.getStartTime().substring(0, 2));
		 int rctime = Integer.parseInt(reserve.getEndTime().substring(0, 2));


		   if ((reserve != null)
				   &&((startTimeChange >= rotime && endTimeChange <= rctime)
						   ||(rotime < startTimeChange && startTimeChange < rctime)
						   ||(rotime < endTimeChange && endTimeChange < rctime)
				   		   ||(startTimeChange == rotime && endTimeChange == rctime))){
			   request.setAttribute("errMsg1", "他のお客様の予約が入っております。");
			   request.setAttribute("coatInfo", coatInfo);
			   request.setAttribute("coatId", coatId);
			   RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coatdetail.jsp");
			   dispatcher.forward(request, response);
		    }
		 }

		   if(date.equals("") || startTime.equals("") || endTime.equals("")) {
			   request.setAttribute("errMsg2", "入力された内容は正しくありません。");
			   request.setAttribute("coatInfo", coatInfo);
			   request.setAttribute("coatId", coatId);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coatdetail.jsp");
				dispatcher.forward(request, response);
			}

		   int otime = Integer.parseInt(coatInfo.getOpenTime().substring(0, 2));
		   int ctime = Integer.parseInt(coatInfo.getClosedTime().substring(0, 2));

		   if((otime > startTimeChange || startTimeChange > ctime)
				   || (otime > endTimeChange || endTimeChange > ctime)){
			   request.setAttribute("errMsg3", "営業時間外です。");
			   request.setAttribute("coatInfo", coatInfo);
			   request.setAttribute("coatId", coatId);
			   RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/coatdetail.jsp");
			   dispatcher.forward(request, response);

		   }

		int price = Integer.parseInt(hourPrice);
		int hour = endTimeChange - startTimeChange;

		int totalPrice = hour * price;

		request.setAttribute("date", date);
		request.setAttribute("time1", startTime);
		request.setAttribute("time2", endTime);
		request.setAttribute("name", name);
		request.setAttribute("totalPrice", totalPrice);
		request.setAttribute("coatId", coatId);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reserveconfirm.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String date = request.getParameter("date");
		String startTime = request.getParameter("time1");
		String endTime = request.getParameter("time2");
		String coatId = request.getParameter("coatId");
		String totalPrice = request.getParameter("totalPrice");
		String id = request.getParameter("id");

		int price = Integer.parseInt(totalPrice);
		int coat = Integer.parseInt(coatId);
		int user = Integer.parseInt(id);
		java.sql.Date  day = java.sql.Date.valueOf(date);

		ReserveDao ReserveDao = new ReserveDao();
		ReserveDate c = new ReserveDate(price,day,startTime,endTime,user,coat);
		ReserveDao.reserve(c);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reserveresult.jsp");
		dispatcher.forward(request, response);
	}

}
