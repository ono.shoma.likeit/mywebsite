package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.ReserveDao;
import beans.ReserveDate;

/**
 * Servlet implementation class ReserveDeleteServlet
 */
@WebServlet("/ReserveCanselServlet")
public class ReserveCanselServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReserveCanselServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String reserveId = request.getParameter("id");

		request.setAttribute("reserveId", reserveId);
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reservecansel.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String reserveId = request.getParameter("reserveId");
		int i = Integer.parseInt(reserveId);

		ReserveDao reserveDao = new ReserveDao();
		ReserveDate c = new ReserveDate(i);

		reserveDao.delete(c);

		response.sendRedirect("IndexServlet");
	}

}
