package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.CoatDate;

public class CoatDao {
	public List<CoatDate> findAll() {
        Connection conn = null;
        List<CoatDate> coatList = new ArrayList<CoatDate>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM coat";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String detail = rs.getString("detail");
                int price = rs.getInt("hour_price");
                String openTime = rs.getString("open_time");
                String closedTime = rs.getString("closed_time");
                String address = rs.getString("address");
                String tel = rs.getString("tel");
                String access = rs.getString("access");
                String fileName1 = rs.getString("file_name1");
                String fileName2 = rs.getString("file_name2");
                String fileName3 = rs.getString("file_name3");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                int sportscategryId = rs.getInt("sports_category_id");

                CoatDate coat = new CoatDate(
                		id, name, detail, price, openTime, closedTime, address, tel,
                		access, fileName1, fileName2, fileName3, createDate, updateDate, sportscategryId);

                coatList.add(coat);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return coatList;
    }
	public CoatDate findById(String targetId) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
           String sql = "SELECT name, detail, hour_price, open_time, closed_time, address, tel,\n" +
           		"access, file_name1, file_name2, file_name3 FROM coat WHERE id = ?";


             // SELECTを実行し、結果表を取得
           PreparedStatement pStmt = conn.prepareStatement(sql);
           pStmt.setString(1, targetId);
           ResultSet rs = pStmt.executeQuery();

             // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }
//            int id = rs.getInt("id");
            String name = rs.getString("name");
            String detail = rs.getString("detail");
            int price = rs.getInt("hour_price");
            String openTime = rs.getString("open_time");
            String closedTime = rs.getString("closed_time");
            String address = rs.getString("address");
            String tel = rs.getString("tel");
            String access = rs.getString("access");
            String fileName1 = rs.getString("file_name1");
            String fileName2 = rs.getString("file_name2");
            String fileName3 = rs.getString("file_name3");

            return new CoatDate(name, detail, price, openTime, closedTime, address, tel,
            		access, fileName1, fileName2, fileName3);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
        	if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	public void coatinsert(CoatDate insertParams) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();
            String sql ="INSERT INTO coat("
            		+ "name,"
            		+ "hour_price,"
            		+ "open_time,"
            		+ "closed_time,"
            		+ "address,"
            		+ "tel,"
            		+ "access,"
            		+ "file_name1,"
            		+ "file_name2,"
            		+ "file_name3,"
            		+ "detail,"
            		+ "sports_category_id,"
            		+ "create_date,"
            		+ "update_date)"
            		+ "VALUE(?,?,?,?,?,?,?,?,?,?,?,?,now(),now())";


            stmt =conn.prepareStatement(sql);

			stmt.setString(1, insertParams.getName());
			stmt.setInt(2, insertParams.getPrice());
			stmt.setString(3, insertParams.getOpenTime());
			stmt.setString(4, insertParams.getClosedTime());
			stmt.setString(5, insertParams.getAddress());
			stmt.setString(6, insertParams.getTel());
			stmt.setString(7, insertParams.getAccess());
			stmt.setString(8, insertParams.getFileName1());
			stmt.setString(9, insertParams.getFileName2());
			stmt.setString(10, insertParams.getFileName3());
			stmt.setString(11, insertParams.getDetail());
			stmt.setInt(12, insertParams.getId());


			stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	public void delete(CoatDate delete) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();
            String sql = "DELETE FROM coat WHERE id=?";
            stmt =conn.prepareStatement(sql);

            stmt.setInt(1, delete.getId());

			stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	public void coatupdate(CoatDate update) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();
            String sql = "UPDATE coat SET "
            		+ "name=?, "
            		+ "hour_price=?, "
            		+ "open_time=? , "
            		+ "closed_time=?, "
            		+ "address=?, "
            		+ "tel=?, "
            		+ "access=?, "
            		+ "file_name1=?, "
            		+ "file_name2=?, "
            		+ "file_name3=?, "
            		+ "detail=?, "
            		+ "update_date=now() "
            		+ "WHERE id=?";
            stmt =conn.prepareStatement(sql);

            stmt.setString(1, update.getName());
			stmt.setInt(2, update.getPrice());
			stmt.setString(3, update.getOpenTime());
			stmt.setString(4, update.getClosedTime());
			stmt.setString(5, update.getAddress());
			stmt.setString(6, update.getTel());
			stmt.setString(7, update.getAccess());
			stmt.setString(8, update.getFileName1());
			stmt.setString(9, update.getFileName2());
			stmt.setString(10, update.getFileName3());
			stmt.setString(11, update.getDetail());
			stmt.setInt(12, update.getId());

			stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	 public List<CoatDate> Info(int spots, String place) {
	        Connection conn = null;
	        PreparedStatement stmt = null;
	        List<CoatDate> coatList = new ArrayList<CoatDate>();
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM coat WHERE sports_category_id =?";

	            if(!place.equals("")) {
	            	sql += " AND address LIKE '%"+place+"%'";
	            }

	             // SELECTを実行し、結果表を取得
	            stmt = conn.prepareStatement(sql);
	            stmt.setInt(1, spots);
	            ResultSet rs = stmt.executeQuery();


	            while (rs.next()) {
	            	int id = rs.getInt("id");
	            	String name = rs.getString("name");
	            	String access = rs.getString("access");
	            	String tel = rs.getString("tel");
	                String address = rs.getString("address");
	                String fileName1 = rs.getString("file_name1");
	                int sports = rs.getInt("sports_category_id");
	                CoatDate coat = new CoatDate(id, name, access, tel, address, fileName1, sports);
	                coatList.add(coat);
	            }

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return coatList;
	    }
}
