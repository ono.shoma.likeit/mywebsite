package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.UserDate;

public class UserDao {

	 public UserDate findByLoginInfo(String loginId, String password) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE login_id = ? and login_password = ?";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, loginId);
	            pStmt.setString(2, password);
	            ResultSet rs = pStmt.executeQuery();


	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	            if (!rs.next()) {
	                return null;
	            }

	            // 必要なデータのみインスタンスのフィールドに追加
	            int id = rs.getInt("id");
	            String loginIdData = rs.getString("login_id");
	            return new UserDate(id,loginIdData);

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }

	 public void insert(UserDate insertParams) {
	        Connection conn = null;
	        PreparedStatement stmt = null;

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();
	            String sql ="INSERT INTO user("
	            		+ "login_id,"
	            		+ "name,"
	            		+ "birth_date,"
	            		+ "login_password,"
	            		+ "create_date,"
	            		+ "update_date)"
	            		+ "VALUES(?,?,?,?,now(),now())";
;

	            stmt =conn.prepareStatement(sql);

	            stmt.setString(1, insertParams.getLoginId());
				stmt.setString(2, insertParams.getName());
				stmt.setDate(3, insertParams.getBirthDate());
				stmt.setString(4, insertParams.getPassword());


				stmt.executeUpdate();

	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	    }
	  public UserDate findByLoginId(String loginId) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE login_id = ?";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, loginId);

	            ResultSet rs = pStmt.executeQuery();


	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	            if (!rs.next()) {
	                return null;
	            }

	            // 必要なデータのみインスタンスのフィールドに追加
	            String loginIdData = rs.getString("login_id");

	            return new UserDate(loginIdData);

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }
	  public UserDate home(String id) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE id = ?";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, id);

	            ResultSet rs = pStmt.executeQuery();


	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	            if (!rs.next()) {
	                return null;
	            }

	            // 必要なデータのみインスタンスのフィールドに追加
	            String loginId = rs.getString("login_id");
	            String name = rs.getString("name");
	            Date birth = rs.getDate("birth_date");
	            String createDate = rs.getString("create_date");

	            return new UserDate(loginId, name, birth, createDate);

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }
	  /**
	     * 全てのユーザ情報を取得する
	     * @return
	     */
	    public List<UserDate> findAll() {
	        Connection conn = null;
	        List<UserDate> userList = new ArrayList<UserDate>();

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            // TODO: 未実装：管理者以外を取得するようSQLを変更する
	            String sql = "SELECT * FROM user";

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String loginId = rs.getString("login_id");
	                String name = rs.getString("name");
	                Date birthDate = rs.getDate("birth_date");
	                String password = rs.getString("login_password");
	                String createDate = rs.getString("create_date");
	                String updateDate = rs.getString("update_date");
	                UserDate user = new UserDate(id, loginId, name, birthDate, password, createDate, updateDate);

	                userList.add(user);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return userList;
	    }
	    public UserDate findById(String id) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE id = ?";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, id);

	            ResultSet rs = pStmt.executeQuery();


	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	            if (!rs.next()) {
	                return null;
	            }

	            // 必要なデータのみインスタンスのフィールドに追加
	            String loginId = rs.getString("login_id");
	            String name = rs.getString("name");
	            Date birth = rs.getDate("birth_date");
	            String createdate = rs.getString("create_date");

	            return new UserDate(loginId,name,birth, createdate);

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }
	    public void delete(UserDate delete) {
	        Connection conn = null;
	        PreparedStatement stmt = null;

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();
	            String sql = "DELETE FROM user WHERE id=?";
	            stmt =conn.prepareStatement(sql);

	            stmt.setInt(1, delete.getId());

				stmt.executeUpdate();

	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	    }
	    public List<UserDate> Info(String id, String name) {
	        Connection conn = null;
	        List<UserDate> userList = new ArrayList<UserDate>();
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE id != 1 ";

	            if(!id.equals("")) {
	            	sql += " AND login_id = '"+id+"'";
	            }
	            if(!name.equals("")) {
	            	sql += " AND name LIKE '%"+name+"%'";
	            }

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.prepareStatement(sql);
	            ResultSet rs = stmt.executeQuery(sql);


	            while (rs.next()) {
	            	 int userid = rs.getInt("id");
	            	 String loginId = rs.getString("login_id");
	                 String nameData = rs.getString("name");
	                 UserDate user = new UserDate(userid,loginId, nameData);
	                 userList.add(user);
	            }

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return userList;
	    }
}
