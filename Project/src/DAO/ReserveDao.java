package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ReserveDate;

public class ReserveDao {

	public void reserve(ReserveDate reserve) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();
            String sql ="INSERT INTO reserve("
            		+ "total_price,"
            		+ "date,"
            		+ "start_time,"
            		+ "end_time,"
            		+ "user_id,"
            		+ "coat_id"
            		+ ")VALUES(?,?,?,?,?,?)";
;

            stmt =conn.prepareStatement(sql);

            stmt.setInt(1, reserve.getPrice());
			stmt.setDate(2, reserve.getDate());
			stmt.setString(3, reserve.getStartTime());
			stmt.setString(4, reserve.getEndTime());
			stmt.setInt(5, reserve.getUserId());
			stmt.setInt(6, reserve.getCoatId());


			stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	public List<ReserveDate> reserveHistory(String id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        List<ReserveDate> reserveList = new ArrayList<ReserveDate>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT "
            		+ " reserve.id,"
            		+ " reserve.date,"
            		+ " reserve.start_time,"
            		+ " reserve.end_time,"
            		+ " reserve.coat_id,"
            		+ " coat.name,"
            		+ " coat.address"
            		+ " FROM reserve "
            		+ " JOIN coat "
            		+ " ON reserve.coat_id = coat.id "
            		+ " WHERE reserve.user_id = ?";

             // SELECTを実行し、結果表を取得
            stmt =conn.prepareStatement(sql);

            stmt.setString(1, id);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
            	int reserveid = rs.getInt("reserve.id");
            	Date date = rs.getDate("reserve.date");
                String time1 = rs.getString("reserve.start_time");
                String time2 = rs.getString("reserve.end_time");
                int coatId = rs.getInt("reserve.coat_id");
                String name = rs.getString("coat.name");
                String address = rs.getString("coat.address");


                ReserveDate reserve = new ReserveDate(reserveid, date, time1, time2, coatId, name, address);

                reserveList.add(reserve);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return reserveList;
    }
	public void delete(ReserveDate delete) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();
            String sql = "DELETE FROM reserve WHERE id=?";
            stmt =conn.prepareStatement(sql);

            stmt.setInt(1, delete.getId());

			stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	public List<ReserveDate> reserve(String coatId,String date) {
        Connection conn = null;
        PreparedStatement stmt = null;
        List<ReserveDate> reserveList = new ArrayList<ReserveDate>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT "
            		+ "*"
            		+ " FROM reserve "
            		+ " WHERE coat_id = ? and date = ?";

             // SELECTを実行し、結果表を取得
            stmt =conn.prepareStatement(sql);

            stmt.setString(1, coatId);
            stmt.setString(2, date);

            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
            int coat = rs.getInt("coat_id");
            Date day = rs.getDate("date");
            String startatime = rs.getString("start_time");
            String closedtime = rs.getString("end_time");

            ReserveDate reserve = new ReserveDate(coat,day,startatime,closedtime);
            reserveList.add(reserve);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return reserveList;
    }
}
