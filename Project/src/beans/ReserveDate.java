package beans;

import java.sql.Date;

public class ReserveDate {
	private int id;
	private int price;
	private Date date;
	private String startTime;
	private String endTime;
	private int userId;
	private int coatId;

	private String name;
	private String address;

	public ReserveDate(int coatId, Date date, String startTime, String endTime) {
		this.coatId = coatId;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
	}


	public ReserveDate(int price, Date date, String startTime, String endTime, int userId, int coatId) {
		this.price = price;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.userId = userId;
		this.coatId = coatId;

	}

	public ReserveDate(int id, Date date, String  startTime, String endTime, int coatId, String name, String address) {
		this.id = id;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.coatId = coatId;
		this.name = name;
		this.address = address;
	}
	public ReserveDate(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCoatId() {
		return coatId;
	}
	public void setCoatId(int coatId) {
		this.coatId = coatId;
	}



}
