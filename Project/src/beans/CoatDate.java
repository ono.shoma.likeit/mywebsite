package beans;

public class CoatDate {
	private int id;
	private String name;
	private String detail;
	private int price;
	private String openTime;
	private String closedTime;
	private String address;
	private String tel;
	private String access;
	private String fileName1;
	private String fileName2;
	private String fileName3;
	private String createDate;
	private String updateDate;
	private int sportscategryId;

	public CoatDate(int id, String name, String detail, int price,
			String openTime, String closeTime, String address,
					String tel, String access, String fileName1, String fileName2, String fileName3,
					String createDate, String updateDate, int sportscategryId) {
		this.id = id;
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.openTime = openTime;
		this.closedTime = closeTime;
		this.address = address;
		this.tel = tel;
		this.access = access;
		this.fileName1 = fileName1;
		this.fileName2 = fileName2;
		this.fileName3 = fileName3;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.sportscategryId = sportscategryId;
	}

	public CoatDate(String name, String detail, int price, String openTime, String closedTime, String address, String tel, String access, String fileName1, String fileName2, String fileName3) {
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.openTime = openTime;
		this.closedTime = closedTime;
		this.address = address;
		this.tel = tel;
		this.access = access;
		this.fileName1 = fileName1;
		this.fileName2 = fileName2;
		this.fileName3 = fileName3;
	}


	public CoatDate(int id) {
		this.id = id;
	}
	public CoatDate(int id, String name, String tel, String access, String address, String fileName1, int sportscategryId) {
		this.id = id;
		this.name = name;
		this.tel = tel;
		this.access = access;
		this.fileName1 = fileName1;
		this.sportscategryId = sportscategryId;
		this.address = address;
	}
	public CoatDate(String name, int price, String openTime, String closedTime, String address, String tel, String access,
			String fileName1, String fileName2, String fileName3, String detail, int id) {
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.openTime = openTime;
		this.closedTime = closedTime;
		this.address = address;
		this.tel = tel;
		this.access = access;
		this.fileName1 = fileName1;
		this.fileName2 = fileName2;
		this.fileName3 = fileName3;
		this.id = id;
	}

	public String getFileName1() {
		return fileName1;
	}

	public void setFileName1(String fileName1) {
		this.fileName1 = fileName1;
	}

	public String getFileName2() {
		return fileName2;
	}

	public void setFileName2(String fileName2) {
		this.fileName2 = fileName2;
	}

	public String getFileName3() {
		return fileName3;
	}

	public void setFileName3(String fileName3) {
		this.fileName3 = fileName3;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String nane) {
		this.name = nane;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}

	public String getClosedTime() {
		return closedTime;
	}

	public void setClosedTime(String closeTime) {
		this.closedTime = closeTime;
	}


	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public int getSportscategryId() {
		return sportscategryId;
	}

	public void setSportscategryId(int sportscategryId) {
		this.sportscategryId = sportscategryId;
	}
}

