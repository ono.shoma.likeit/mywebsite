<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>管理者ホーム</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header class="header">
        <div>
           <a href="IndexServlet" class="header2" color="black">レンタルコート場</a>
            <li align="right"><a href="UserHomeServlet?id=${userInfo.id}">ホーム</a></li>
            <li align="right"><a href="LogoutServlet" class="red">ログアウト</a></li>
        </div>
    </header>

    <h3 align="center">マスタホーム画面</h3>
    <form action="MasterHomeServlet"method="post">
    <div align="center">
        <div class="mt-5">ユーザーID</div>
        <div><input type="text" name="id"></div>
        <div>ユーザー名</div>
        <div><input type="text" name="name"></div>
    </div>
    <div class="mt-5" align="center">
        <input type="submit" class="padding50" value="検索">
    </div>
    </form>
    <div class="ccc">
        <form action="CoatRegistServlet"method="get">
            <input type="submit" value="コート登録">
        </form>
    </div>
    <div class="mt-5">
        <table border="1" align="center">
            <tr>
                <th width="200">ユーザーID</th>
                <th width="250">ユーザー名</th>
                <th width="250"></th>
            </tr>
            <c:forEach var="user" items="${userList}">
           	 <tr>
                <td>${user.loginId}</td>
                <td>${user.name}</td>
                <td>
                    <a href="UserDetailServlet?id=${user.id }" class="btn btn-primary">詳細</a>
                    <a href="UserDeleteServlet?id=${user.id }" class="btn btn-danger">削除</a>
                </td>
             </tr>
            </c:forEach>
        </table>
    </div>
</body>

</html>
