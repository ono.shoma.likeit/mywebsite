<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>index</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header class="header">
        <div>
            <a href="IndexServlet" class="header2" color="black">レンタルコート場</a>
            <div align="left" class="marginl1235">
             <c:if test="${userInfo.loginId == 'admin'}">
                <li><a href="MasterHomeServlet">マスタホーム</a></li>
             </c:if>
                <li><a href="UserHomeServlet?id=${userInfo.id}">ホーム</a></li>
                <li><a href="LogoutServlet" class="red">ログアウト</a></li>
            </div>
        </div>
    </header>
    <form action="IndexServlet"method="post">
        <div class="ccc">
            <p>
                <font size="4">スポーツ</font><br>
                <input type="radio" name="sports" value="1" checked="checked">サッカー
		        <input type="radio" name="sports" value="2">野球
		        <input type="radio" name="sports" value="3">バスケ
		        <input type="radio" name="sports" value="4">バレーボール
		        <input type="radio" name="sports" value="5">バドミントン
            </p>
        </div>
        <div class="ccc">
            <p>
                <font size="4">場所</font>
            </p>
            <input type="text" name="address">
            <input type="submit" value="検索">
        </div>
    </form>
    <table align="center" border="1" class="margin-top">
        <tr>
            <th width="300">イメージ</th>
            <th width="600">概要</th>
        </tr>
        <c:forEach var="coat" items="${coatList}">
        <tr>
            <td>
                <img src="img/${coat.fileName1 }"width="300" height="200">
            </td>
            <td>
                <div style="margin-bottom:10px">${coat.name }</div>
                <div>
                    アクセス：${coat.access }
                    <br>住所：${coat.address }
                    <br>電話番号： ${coat.tel }
                </div>
                <div class="margin250">
                <c:if test="${userInfo.loginId == 'admin'}">
                    <a href="CoatUpdateServlet?id=${coat.id}" class="btn btn-success">更新</a>
                    <a href="CoatDeleteServlet?id=${coat.id}" class="btn btn-danger">削除</a>
                </c:if>
                    <a href="ReserveSevlet?id=${coat.id}" class="btn btn-primary">予約</a>
                </div>
            </td>
        </tr>
        </c:forEach>
    </table>
</body></html>
