<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>コート情報更新</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
   <header class="header">
        <div>
             <a href="IndexServlet" class="header2" color="black">レンタルコート場</a>
             <li align="right"><a href="UserHomeServlet?id=${userInfo.id}">ホーム</a></li>
            <li align="right"><a href="LogoutServlet" class="red">ログアウト</a></li>
        </div>
    </header>

    <h3 align="center">コート情報更新</h3>
    <form action="CoatUpdateServlet"method="post">
        <div>
            <table align="center" border="1" class="mt-5">
                <tr>
                    <th width="150">名前</th>
                    <td width="600"><input type="text" style="border:none" size="70" name="coatname" value="${coat.name }"></td>
                </tr>
                <tr>
                    <th>料金（1時間）</th>
                    <td><input type="text" style="border:none" size="70" name="hourprice" value="${coat.price }"円></td>
                </tr>
                <tr>
                    <th>営業時間</th>
                    <td><input type="time" name="time1"value="${coat.openTime }">~<input type="time" name="time2"value="${coat.closedTime }"></td>
                </tr>
                <tr>
                    <th>住所</th>
                    <td><input type="text" style="border:none" name="address" size="70" value="${coat.address }"></td>
                </tr>
                <tr>
                    <th>電話番号</th>
                    <td><input type="tel" style="border:none" size="70" name="tel" value="${coat.tel }"></td>
                </tr>
                <tr>
                    <th>アクセス</th>
                    <td><input type="text" style="border:none" size="70" name="access" value="${coat.access }"></td>
                </tr>
                <tr>
                    <th>写真1</th>
                    <td><input type="file" name="img1" value="${coat.fileName1 }"></td>
                </tr>
                <tr>
                    <th>写真2</th>
                    <td><input type="file" name="img2" value="${coat.fileName2 }"></td>
                </tr>
                <tr>
                    <th>写真3</th>
                    <td><input type="file" name="img3" value="${coat.fileName3 }"></td>
                </tr>
                <tr>
                    <th>詳細</th>
                    <td height="300">
                        <textarea type="text" style="border:none" rows="12" cols="100" name="detail">
                        ${coat.detail }
                        </textarea>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center" class="mt-5">
         	<input type="hidden" name="coatId" value="${id}">
            <input type="submit" value="更新">
        </div>
    </form>
</body></html>
