<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>新規登録完了</title>
   <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="header">
        <div class="header2" color="black">
            レンタルコート場
        </div>
    </header>
    <h2 align="center" class="marginb">登録完了しました。</h2>
    <p align="center"><a href="LoginServlet">ログイン画面へ</a></p>
</body>

</html>
