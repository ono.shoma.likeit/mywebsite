<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ホーム画面</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header class="header">
        <div>
             <a href="IndexServlet" class="header2" color="black">レンタルコート場</a>
             <li align="right"><a href="UserHomeServlet?id=${userInfo.id}">ホーム</a></li>
            <li align="right"><a href="LogoutServlet" class="red">ログアウト</a></li>
        </div>
    </header>

    <h3 align="center" class="margint20">ホーム画面</h3>
    <div align="center">
        <table border="1" class="p-3 mb-2 bg-secondary text-white">
            <tr>
                <th width="200">ログインID</th>
                <td width="300">${user.loginId}</td>
            </tr>
            <tr>
                <th>名前</th>
                <td>${user.name}</td>
            </tr>
            <tr>
                <th>生年月日</th>
                <td>${user.birthDate}</td>
            </tr>
            <tr>
                <th>登録日</th>
                <td>${user.createDate}</td>
            </tr>
        </table>
    </div>
    <div align="center" class="margint100">
        <div>
            <h5>レンタル履歴</h5>
        </div>
        <table class="margint20 table-css">
            <tr>
                <th width="100" class="t-css"></th>
                <th width="100" class="td-css">日付</th>
                <th width="100" class="td-css">時間</th>
                <th width="200" class="td-css">場所</th>
                <th width="400" class="td-css">住所</th>
                <th width="100" class="td-css"></th>
            </tr>
            <c:forEach var="reserve" items="${reserveList}">
            <tr>
                <td class="t-css"><a href="ReserveSevlet?id=${reserve.coatId }&detail=あ" class="btn btn-info">詳細</a></td>
                <td height="50" class="td-css">${reserve.date }</td>
                <td class="td-css">${reserve.startTime }~${reserve.endTime }</td>
                <td class="td-css">${reserve.name }</td>
                <td class="td-css">${reserve.address }</td>
                <td class="td-css"><a href="ReserveCanselServlet?id=${reserve.id}" class="btn btn-danger">取り消し</a></td>
            </tr>
            </c:forEach>
        </table>
    </div>
</body>

</html>
