<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
    <header class="header">
        <div class="header2"color="black">
         レンタルコート場
        </div>
    </header>
    <h2 align="center">ログイン画面</h2>
    <div align="center">
        <div class =box>
          <form action="LoginServlet" method="post">
              <p class="fontlarge">ログインID</p>
               <input type="text" name="id">
              <p class="fontlarge">パスワード</p>
               <input type="password" name="pass">
              <div class="margin-top">
                  <input type="submit"value="ログイン"></div>
            </form>
        </div>
    </div>
    <div align="right"class=toroku><a href="UserRegistServlet">新規登録</a></div>
    <footer class="header">
    </footer>
</body>
</html>