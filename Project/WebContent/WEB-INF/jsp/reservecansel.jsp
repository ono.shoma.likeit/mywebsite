<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>予約取り消し</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header class="header">
        <div>
            <a href="index.html" class="header2" color="black">レンタルコート場</a>
            <li align="right"><a href="UserHomeServlet?id=${userInfo.id}">ホーム</a></li>
            <li align="right"><a href="LogoutServlet" class="red">ログアウト</a></li>
        </div>
    </header>

    <div align="center" class="margin-top">
        <h5>2020-09-09の#####サッカーコートでの予約を取り消しますか？</h5>
    </div>
    <table align="center" class="margin-top">
        <tr>
            <td>
                <form action="UserHomeServlet" method="get">
                	 <input type="hidden" name="id" value="${userInfo.id}">
                    <input type=submit class="padding50" value="いいえ">
                </form>
            </td>
            <td>
                <form action="ReserveCanselServlet" method="post">
                	<input type="hidden" name="reserveId" value="${reserveId}">
                    <input type=submit class="padding50" value="はい">
                </form>
            </td>
        </tr>
    </table>
