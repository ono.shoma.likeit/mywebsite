<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>予約完了</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header class="header">
        <div>
            <a href="IndexServlet" class="header2" color="black">レンタルコート場</a>
           <li align="right"><a href="UserHomeServlet?id=${userInfo.id}">ホーム</a></li>
            <li align="right"><a href="login.html" class="red">ログアウト</a></li>
        </div>
    </header>

    <h3 align="center" class="margint100">予約が完了しました。</h3>
    <h4 align="center">
        <p>ありがとうございました。</p>
    </h4>
    <div align="center">
        <img src="img/thanks.jpg" alt="######サッカーコート" title="サッカーコート" width="200" height="600">
    </div>
</body>

</html>
