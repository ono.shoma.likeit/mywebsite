<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>予約確認</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header class="header">
        <div>
             <a href="IndexServlet" class="header2" color="black">レンタルコート場</a>
             <li align="right"><a href="UserHomeServlet?id=${userInfo.id}">ホーム</a></li>
            <li align="right"><a href="LogoutServlet" class="red">ログアウト</a></li>
        </div>
    </header>

    <h2 align="center">予約確認</h2>
    <div align="center" class="margin-top">
        <table border="1" class="table-active">
            <tr>
                <td width="150">料金</td>
                <td width="500">${totalPrice}円</td>
            </tr>
            <tr>
                <td>日程</td>
                <td>${date}</td>
            </tr>
            <tr>
                <td>時間</td>
                <td>${time1}〜${time2}</td>
            </tr>
            <tr>
                <td>場所</td>
                <td>${name}</td>
            </tr>
        </table>
    </div>
    <div class="mt-5">
        <div align="center">以上の内容で予約してもよろしいでしょうか？</div>
    </div>
    <div class="mt-5">
        <table align="center" border="0">
            <tr>
                <td>
                    <form action="IndexServlet">
                        <input type="submit" class="padding50" value="いいえ">
                    </form>
                </td>
                <td>
                    <form action="ReserveConfirmServlet" method="post">
                        <input type="hidden" name="coatId" value="${coatId}">
                        <input type="hidden" name="date" value="${date}">
                        <input type="hidden" name="time1" value="${time1}">
                        <input type="hidden" name="time2" value="${time2}">
                        <input type="hidden" name="totalPrice" value="${totalPrice}">
                        <input type="hidden" name="id" value="${userInfo.id}">
                        <input type="submit" class="padding50" value="はい">
                    </form>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>
