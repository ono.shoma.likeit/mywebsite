<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>新規登録</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="header">
        <div class="header2" color="black">
            レンタルコート場
        </div>
    </header>
    <h2 align="center">新規登録</h2>
    <p align="center"class="red">${errMsg}</p>
    <form action="UserRegistServlet" method="Post">
        <table align="center" border="0">
            <tr align="center" class=height>
                <td width="400">ログインID</td>
                <td width="400"><input type="text" name="id"></td>
            </tr>
            <tr align="center" class=height>
                <td width="400">パスワード</td>
                <td width="400"><input type="text" name="pass"></td>
            </tr>
            <tr align="center" class=height>
                <td width="400">パスワード（確認）</td>
                <td width="400"><input type="text" name="pass2"></td>
            </tr>
            <tr align="center" class=height>
                <td width="400">ユーザー名</td>
                <td width="400"><input type="text" name="name"></td>
            </tr>
            <tr align="center" class=height>
                <td width="400">生年月日</td>
                <td width="400"><input type="date" name="birth_date"></td>
            </tr>
        </table>
        <div align="center"><input type="submit" class=padding50 value="登録"></div>
    </form>
    <div class=user><a href="LoginServlet">戻る</a></div>
</body>

</html>
