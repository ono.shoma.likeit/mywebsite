<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>コート場詳細</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
    <header class="header">
        <div>
            <a href="IndexServlet" class="header2" color="black">レンタルコート場</a>
            <li align="right"><a href="UserHomeServlet?id=${userInfo.id}">ホーム</a></li>
            <li align="right"><a href="LogoutServlet" class="red">ログアウト</a></li>
        </div>
    </header>
    <div class="row">
        <div class="picsize blocktext float">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="img/${coatInfo.fileName1 }" class="d-block" width="600" height="400" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="img/${coatInfo.fileName2 }"class="d-block" width="600" height="400" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="img/${coatInfo.fileName3 }" class="d-block" width="600" height="400" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="float picsize marginl">
            <h1>${coatInfo.name }</h1>
            <p>${coatInfo.detail }</p>
        </div>
    </div>
    <div class="blocktext float">
        <table border="1" class="p-3 mb-2 bg-secondary text-white">
            <tr>
                <th width="150"></th>
                <th width="500"></th>
            </tr>
            <tr>
                <td>料金（1時間）</td>
                <td>${coatInfo.price }円</td>
            </tr>
            <tr>
                <td>営業時間</td>
                <td>${coatInfo.openTime }~${coatInfo.closedTime }</td>
            </tr>
            <tr>
                <td>アクセス</td>
                <td>${coatInfo.access }</td>
            </tr>
            <tr>
                <td>住所・電話番号</td>
                <td>住所： ${coatInfo.address }
                    <br>電話番号： ${coatInfo.tel }
                </td>
            </tr>
        </table>
    </div>
    <c:if test="${detail == null}">
    <form action="ReserveConfirmServlet" method="get">
        <div class="float marginl margint165">
        <div class="red">${errMsg1 }${errMsg2 }${errMsg3 }</div>
            <table border="0">
                <tr>
                    <td width="50">日程</td>
                    <td width="150"><input type="date" name="date"></td>
                </tr>
                <tr>
                    <td width="50">時間</td>
                    <td width="200"><input type="time" name="time1" step="3600">〜<input type="time" name="time2" step="3600"></td>
                </tr>
            </table>
        </div>
        <div class="float margint200">
        <input type="hidden" name="id" value="${coatId}">
        <input type="hidden" name="name" value="${coatInfo.name}">
        <input type="hidden" name="price" value="${coatInfo.price}">
            <input type="submit" value="予約する">
        </div>
    </form>
    </c:if>
</body>

</html>
