<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザー削除</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header class="header">
        <div>
            <a href="index.html" class="header2" color="black">レンタルコート場</a>
            <li align="right"><a href="UserHomeServlet?id=${userInfo.id}">ホーム</a></li>
            <li align="right"><a href="LogoutServlet" class="red">ログアウト</a></li>
        </div>
    </header>

    <h3 align="center">ユーザー削除</h3>
    <p align="center" class="mt-5">${user.name}さんを削除しますか？</p>
    <div align="center" class="mt-5">
        <table border"0">
            <tr>
                <td>
                    <form action="UserDeleteServlet" method="post">
                    	<input type="hidden" name="id" value="${id}">
                        <input type=submit class="pudding50" value="はい">
                    </form>
                </td>
                <td>
                    <form action="MasterHomeServlet" method="get">
                        <input type=submit class="pudding50" value="キャンセル">
                    </form>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>
