<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>新規登録確認</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="header">
        <div class="header2" color="black">
            レンタルコート場
        </div>
    </header>
    <h2 align="center">登録確認</h2>
    <h4 align="center">この内容で登録してもよろしいでしょうか。</h4>

    <table align="center" border="0">
        <tr align="left" class=height>
            <td>ログインID</td>
            <td>
                <p class="ccc">${id}</p>
            </td>
        </tr>
        <tr align="left" class=height>
            <td>ユーザー名</td>
            <td>
                <p class="ccc">${name}</p>
            </td>
        </tr>
        <tr align="left" class="height marginl">
            <td>生年月日</td>
            <td>
                <p class="ccc">${birth}</p>
            </td>
        </tr>

        <tr align="center" class=height>
            <td width="200">
                <form action="regist.html">
                    <div align="center"><input type="submit" class=padding50 value="いいえ"></div>
                </form>
            </td>
            <td width="200">
                <form action="UserRegistconfirm" method="Post">
                    <input type="hidden" name="id" value="${id}">
                    <input type="hidden" name="name" value="${name}">
                    <input type="hidden" name="pass" value="${pass}">
                    <input type="hidden" name="birth" value="${birth}">
                    <div align="center"><input type="submit" class="padding50" value="登録"></div>
                </form>
            </td>
        </tr>
    </table>
    <div class=user><a href="UserListServlet">戻る</a></div>
</body>

</html>
