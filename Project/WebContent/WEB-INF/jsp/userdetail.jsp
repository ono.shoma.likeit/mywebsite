<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>管理者ホーム</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header class="header">
        <div>
             <a href="IndexServlet" class="header2" color="black">レンタルコート場</a>
             <li align="right"><a href="UserHomeServlet?id=${userInfo.id}">ホーム</a></li>
            <li align="right"><a href="login.html" class="red">ログアウト</a></li>
        </div>
    </header>
    <h2 align="center">ユーザー詳細</h2>
    <table align="center" border="0">
        <tr align="left" class=height>
            <td>ログインID</td>
            <td>
                <div class="ccc">${user.loginId}</div>
            </td>
        </tr>
        <tr align="left" class=height>
            <td>ユーザー名</td>
            <td>
                <div class="ccc">${user.name}</div>
            </td>
        </tr>
        <tr align="left" class="height marginl">
            <td>生年月日</td>
            <td>
                <div class="ccc">${user.birthDate}</div>
            </td>
        </tr>
        <tr align="left" class="height marginl">
            <td>登録日</td>
            <td>
                <div class="ccc">${user.createDate}</div>
            </td>
        </tr>
    </table>
</body></html>
