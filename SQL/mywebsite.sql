
CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8

USE mywebsite;

CREATE TABLE reserve (
id int PRIMARY KEY AUTO_INCREMENT,
reserve_price INT NOT NULL,
reserve_date DATE NOT NULL,
reserve_satrt_time TIME NOT NULL,
reserve_end_time TIME NOT NULL,
user_id int NOT NULL,
coat_id int NOT NULL
)

CREATE TABLE coat (
id int PRIMARY KEY AUTO_INCREMENT,
coat_name VARCHAR(50) NOT NULL,
coat_detail TEXT NOT NULL,
coat_price int NOT NULL,
coat_open TIME NOT NULL,
coat_closed TIME NOT NULL,
coat_address VARCHAR(250) NOT NULL,
coat_tel int NOT NULL,
coat_access VARCHAR(250) NOT NULL,
coat_create_date DATETIME NOT NULL,
coat_update_date DATETIME NOT NULL,
sports_category_id VARCHAR(250) NOT NULL
)

CREATE TABLE user (
id int PRIMARY KEY AUTO_INCREMENT,
user_loginid VARCHAR(25) UNIQUE NOT NULL,
user_name VARCHAR(25) NOT NULL,
user_birthdate DATE NOT NULL,
user_loginpassword VARCHAR(25) NOT NULL,
user_create_date DATETIME NOT NULL,
user_update_date DATETIME NOT NULL
)

CREATE TABLE spots_category (
id int PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(255) NOT NULL
);
